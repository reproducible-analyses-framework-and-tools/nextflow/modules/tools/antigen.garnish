#!/usr/bin/env nextflow

process antigen_garnish_foreignness {
// Runs antigen.garnish foreignness calculation
//
// input:
//   tuple
//     val(pat_name) - Patient Name
//     val(norm_run) - Normal Run Name
//     val(tumor_run) - Tumor Run Name
//     val(dataset) - Dataset
//     path(peptides_file) - Peptides File
//   path(ag_data_dir) - antigen.garnish Data Directory
//   val(species) - Sample Species
//
// output:
//   tuple => foreignness_files
//     val(pat_name) - Patient Name
//     val(norm_run) - Normal Run Name
//     val(tumor_run) - Tumor Run Name
//     val(dataset) - Dataset
//     path("*agg.ag_foreign.tsv") - antigen.garnish Foreignness File

  tag "${dataset}/${pat_name}/${norm_run}_${tumor_run}"
  label 'antigen_garnish_container'
  label 'antigen_garnish_foreignness'
  cache 'lenient'

  input:
  tuple val(pat_name), val(norm_run), val(tumor_run), val(dataset), path(peptides_file)
  path ag_data_dir
  val species

  output:
  tuple val(pat_name), val(norm_run), val(tumor_run), val(dataset), path("*agg.ag_foreign.tsv"), emit: foreignness_files

  script:
  """
  echo "Identifier	Peptide" > ag_peps.tsv
  awk -v RS=">" -v ORS="\n" -v OFS="" '{\$1=""\$1"\t"}1' ${peptides_file} | tail -n+2 >> ag_peps.tsv

  NUM_LINES=`wc -l ag_peps.tsv | cut -f 1 -d ' '`
  AVG_NUM_LINES=\$((\${NUM_LINES}/${task.cpus}))

  tail -n +2 ag_peps.tsv | split -d -l \${AVG_NUM_LINES} - --filter='sh -c "{ head -n1 ag_peps.tsv; cat; } > \$FILE"'

  for i in `ls x*`; do
    export PATH=\$PATH:"${ag_data_dir}"
    export AG_DATA_DIR="${ag_data_dir}"
    echo "#!/usr/bin/env Rscript" > CMD_\${i}
    echo "library(magrittr)" >> CMD_\${i}
    echo "library(data.table)" >> CMD_\${i}
    echo "library(antigen.garnish)" >> CMD_\${i}
    echo "peptide_table <- read.table(file = '\${i}', header=TRUE, sep='\\t')" >> CMD_\${i}
    echo "peptides <- as.vector(peptide_table\\\$Peptide)" >> CMD_\${i}
    echo "foreigns <- peptides %>% foreignness_score(db = '${species}')" >> CMD_\${i}
    echo "write.table(foreigns, file = '${dataset}-${pat_name}-${norm_run}_${tumor_run}.\${i}.ag_foreign.tsv', row.names=FALSE, quote=FALSE, sep=',')" >> CMD_\${i}
    chmod +x CMD_\${i}
  done
  for j in `ls CMD_x*`; do
    Rscript \${j}&
  done
  wait

  head -1 ${dataset}-${pat_name}-${norm_run}_${tumor_run}.x00.ag_foreign.tsv > ${dataset}-${pat_name}-${norm_run}_${tumor_run}.agg.ag_foreign.tsv
  tail -n +2 ${dataset}-${pat_name}-${norm_run}_${tumor_run}.x*.ag_foreign.tsv >> ${dataset}-${pat_name}-${norm_run}_${tumor_run}.agg.ag_foreign.tsv
  """
}


process antigen_garnish_foreignness_rna {
// Runs antigen.garnish foreignness calculation
//
// input:
//   tuple
//     val(pat_name) - Patient Name
//     val(norm_run) - Run Name
//     val(dataset) - Dataset
//     path(peptides_file) - Peptides File
//   path(ag_data_dir) - antigen.garnish Data Directory
//   val(species) - Sample Species
//
// output:
//   tuple => foreignness_files
//     val(pat_name) - Patient Name
//     val(tumor_run) - Run Name
//     val(dataset) - Dataset
//     path("*agg.ag_foreign.tsv") - antigen.garnish Foreignness File

  tag "${dataset}/${pat_name}/${run}"
  label 'antigen_garnish_container'
  label 'antigen_garnish_foreignness'
  cache 'lenient'

  input:
  tuple val(pat_name), val(run), val(dataset), path(peptides_file)
  path ag_data_dir
  val species

  output:
  tuple val(pat_name), val(run), val(dataset), path("*ag_foreign.tsv"), emit: foreignness_files

  script:
  """
  export PATH=\$PATH:"${ag_data_dir}"
  export AG_DATA_DIR="${ag_data_dir}"
  echo "#!/usr/bin/env Rscript" > CMD
  echo "library(magrittr)" >> CMD
  echo "library(data.table)" >> CMD
  echo "library(antigen.garnish)" >> CMD
  echo "peptide_table <- read.table(file = '${peptides_file}', header=TRUE, sep='\\t')" >> CMD
  echo "peptides <- as.vector(peptide_table\\\$Peptide)" >> CMD
  echo "foreigns <- peptides %>% foreignness_score(db = '${species}')" >> CMD
  echo "write.table(foreigns, file = '${dataset}-${pat_name}-${run}.ag_foreign.tsv', row.names=FALSE, quote=FALSE, sep=',')" >> CMD
  chmod +x CMD
  Rscript CMD
  """
}


process antigen_garnish_dissimilarity {
// Runs antigen.garnish dissimilarity calculation
//
// input:
//   tuple
//     val(pat_name) - Patient Name
//     val(norm_run) - Normal Run Name
//     val(tumor_run) - Tumor Run Name
//     val(dataset) - Dataset
//     path(peptides_file) - Peptides File
//   path(ag_data_dir) - antigen.garnish Data Directory
//   val(species) - Sample Species
//
// output:
//   tuple => foreignness_files
//     val(pat_name) - Patient Name
//     val(norm_run) - Normal Run Name
//     val(tumor_run) - Tumor Run Name
//     val(dataset) - Dataset
//     path("*agg.ag_dissim.tsv") - antigen.garnish Dissimilarity File

  tag "${dataset}/${pat_name}/${norm_run}_${tumor_run}"
  label 'antigen_garnish_container'
  label 'antigen_garnish_dissimilarity'
  cache 'lenient'


  input:
  tuple val(pat_name), val(norm_run), val(tumor_run), val(dataset), path(peptides_file)
  path ag_data_dir
  val species

  output:
  tuple val(pat_name), val(norm_run), val(tumor_run), val(dataset), path("*ag_dissim.tsv"), emit: dissimilarity_files

  script:
  """
  echo "Identifier	Peptide" > ag_peps.tsv
  awk -v RS=">" -v ORS="\n" -v OFS="" '{\$1=""\$1"\t"}1' ${peptides_file} | tail -n+2 >> ag_peps.tsv

  export PATH=\$PATH:"${ag_data_dir}"
  export AG_DATA_DIR="${ag_data_dir}"
  echo "#!/usr/bin/env Rscript" > CMD
  echo "library(magrittr)" >> CMD
  echo "library(data.table)" >> CMD
  echo "library(antigen.garnish)" >> CMD
  echo "peptide_table <- read.table(file = 'ag_peps.tsv', header=TRUE, sep='\\t')" >> CMD
  echo "peptides <- as.vector(peptide_table\\\$Peptide)" >> CMD
  echo "dissims <- peptides %>% dissimilarity_score(db = '${species}')" >> CMD
  echo "write.table(dissims, file = '${dataset}-${pat_name}-${norm_run}_${tumor_run}.ag_dissim.tsv', row.names=FALSE, quote=FALSE, sep=',')" >> CMD
  chmod +x CMD
  Rscript CMD
  """
}


process antigen_garnish_dissimilarity_rna {
// Runs antigen.garnish dissimilarity calculation
//
// input:
//   tuple
//     val(pat_name) - Patient Name
//     val(run) - Run Name
//     val(dataset) - Dataset
//     path(peptides_file) - Peptides File
//   path(ag_data_dir) - antigen.garnish Data Directory
//   val(species) - Sample Species
//
// output:
//   tuple => foreignness_files
//     val(pat_name) - Patient Name
//     val(run) - Run Name
//     val(dataset) - Dataset
//     path("*agg.ag_dissim.tsv") - antigen.garnish Dissimilarity File

  tag "${dataset}/${pat_name}/${run}"
  label 'antigen_garnish_container'
  label 'antigen_garnish_dissimliarity'
  cache 'lenient'

  input:
  tuple val(pat_name), val(run), val(dataset), path(peptides_file)
  path ag_data_dir
  val species

  output:
  tuple val(pat_name), val(run), val(dataset), path("*ag_dissim.tsv"), emit: dissimilarity_files

  script:
  """
  export PATH=\$PATH:"${ag_data_dir}"
  export AG_DATA_DIR="${ag_data_dir}"
  echo "#!/usr/bin/env Rscript" > CMD
  echo "library(magrittr)" >> CMD
  echo "library(data.table)" >> CMD
  echo "library(antigen.garnish)" >> CMD
  echo "peptide_table <- read.table(file = '${peptides_file}', header=TRUE, sep='\\t')" >> CMD
  echo "peptides <- as.vector(peptide_table\\\$Peptide)" >> CMD
  echo "dissims <- peptides %>% dissimilarity_score(db = '${species}')" >> CMD
  echo "write.table(dissims, file = '${dataset}-${pat_name}-${run}.ag_dissim.tsv', row.names=FALSE, quote=FALSE, sep=',')" >> CMD
  chmod +x CMD
  Rscript CMD
  """
}


process antigen_garnish_make_input_file {
// Makes antigen.garnish input file
//
// input:
//   tuple
//     val(pat_name) - Patient Name
//     val(norm_run) - Normal Run Name
//     val(tumor_run) - Tumor Run Name
//     val(dataset) - Dataset
//     path(peptides_file) - Peptides File
//   val(comma_sep_lens) - Comma-separated List of Peptide Lengths
//
// output:
//   tuple => foreignness_files
//     val(pat_name) - Patient Name
//     val(norm_run) - Normal Run Name
//     val(tumor_run) - Tumor Run Name
//     val(dataset) - Dataset
//     path("*ag_inp.tsv") - antigen.garnish Input File

  tag "${dataset}/${pat_name}/${norm_run}_${tumor_run}"
  cache 'lenient'


  input:
  tuple val(pat_name), val(norm_run), val(tumor_run), val(dataset), path(peptides_file)
  val comma_sep_lens

  output:
  tuple val(pat_name), val(norm_run), val(tumor_run), val(dataset), path("*ag_inp.tsv"), emit: ag_inps

  script:
  """
#!/usr/bin/env python3

import itertools
import re

peptide_lens = [int(x) for x in "${comma_sep_lens}".split(',')]
print("Peptide lengths: {}".format(peptide_lens))


peptides = {}
with open("${peptides_file}", 'r') as pep_inf:
    for line in pep_inf:
       if re.search('^>', line):
           ident = line.rstrip().split(' ')[0]
           nl = next(pep_inf)
           if nl.startswith(';'):
             peptides[ident] = next(pep_inf).rstrip()
           else:
             peptides[ident] = nl.rstrip()

def sliding_window(iterable, n):
    iterables = itertools.tee(iterable, n)

    for iterable, num_skipped in zip(iterables, itertools.count()):
        for _ in range(num_skipped):
            next(iterable, None)

    joind_peps = [''.join(x) for x in zip(*iterables)]

    return list(set(joind_peps))

pep_kmers = {}

for id,pep in peptides.items():
    pep_kmers[id] = []
    for peptide_len in [x for x in peptide_lens if x <= len(pep)]:
        pep_kmers[id].extend(sliding_window(pep, peptide_len))

with open("${dataset}-${pat_name}-${norm_run}_${tumor_run}.ag_inp.tsv", 'w') as outf:
    outf.write("Identifier\\tPeptide\\n")
    for id, id_pep_kmer in pep_kmers.items():
        for sub_pep_kmer in id_pep_kmer:
            outf.write("{}\\t{}\\n".format(id, sub_pep_kmer))
  """
}
